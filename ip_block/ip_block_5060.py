#!/usr/bin/env python

'''
@author: ivanrajkovic@gmail.com
'''
import os
import subprocess
import time
import datetime
import http.server
import socketserver
import threading
import sys
import signal
import socket

web_dir = '/srv/http/'


html_file = os.path.join(web_dir,'index.html')
log_file = os.path.join(web_dir,'log.txt')


# html file 

html_top = """<!DOCTYPE html>
<html>
<head>
<meta http-equiv="refresh" content="100">
</head>
<body>
"""

html_end="""</body>
</html>"""

# create an empty dictionary
bl_ips = {}

while 1:
    if os.path.exists(log_file):
        with open(log_file) as old_log_file:
            old_log = old_log_file.read()
        if len(old_log)> 30000:
            old_log=old_log[:5000]
    else:
        old_log = ''
    
    new_log='Time: {}<br>\n'.format(f"{datetime.datetime.now():%Y-%m-%d %H:%M:%S}")
    # get info about udp connections to port 5060
    con_out = subprocess.check_output(['conntrack','-L', '-p','udp','--orig-port-dst','5060']).decode("utf-8")
    
    # set empty list of IP addresses
    ips = []
    
    # go through all the connections, find the ones not started by 192.168.12.53 going to 5060
    # append them to the list
    for line in con_out.splitlines():
        if ('dport=5060 ' in line) and ('src=192.168.12.53' != line.split()[3]):
            ips.append(line.split()[3][4:])
    
    # find unique IP addresses
    uni_ips = list(set(ips))
    
    # sort ips
    b_ips = list(map(lambda x: socket.inet_aton(x),uni_ips[:]))
    b_ips.sort()
    uni_ips_sorted = list(map(lambda x: socket.inet_ntoa(x),b_ips[:]))
    
    iptables_changed = 0
    
    # go through unique IP addresses, ban the ones that show more than 2 times in the original list
    for ip in uni_ips_sorted:
        ip_c = ips.count(ip) 
        if ip_c > 2:
            subprocess.Popen(['iptables','-I','fw-open','-s',ip,'-j','DROP'])
            subprocess.Popen(['conntrack','-D','-s',ip])
            new_log = new_log+'<b>Banning {}</b>, it has {} connections<br>'.format(ip,ip_c)
            iptables_changed = 1
        else:
            new_log = new_log+'IP {} has {} connection(s)<br>\n'.format(ip,ip_c)
    
    # get the subnet list, remake ips list after blocking
    if iptables_changed==1:
        con_out = subprocess.check_output(['conntrack','-L', '-p','udp','--orig-port-dst','5060']).decode("utf-8")
        ips=[]
        for line in con_out.splitlines():
            if ('dport=5060 ' in line) and ('src=192.168.12.53' != line.split()[3]):
                ips.append(line.split()[3][4:]) 
        
    subnet_list = list(map(lambda x : '.'.join(x.split('.')[:-1]),ips[:]))
    uni_subnet_list = list(set(subnet_list))
    
    # sort unique subnets
    b_sub = list(map(lambda x: socket.inet_aton(x+'.0'),uni_subnet_list[:]))
    b_sub.sort()
    uni_sub_sorted = list(map(lambda x: '.'.join(socket.inet_ntoa(x).split('.')[:-1]),b_sub[:]))

    # check if the /24 submask should be blocked
    for sub in uni_sub_sorted:
        sub_c = subnet_list.count(sub)
        if sub_c > 2:
            subprocess.Popen(['iptables','-I','fw-open','-s',sub+'.0/24','-j','DROP'])
            for ip in ips:
                if sub in ip:
                    subprocess.Popen(['conntrack','-D','-s',ip])
            new_log = new_log+'<b>Banning {}.0/24</b>, it has {} connections<br>\n'.format(sub,sub_c)
            iptables_changed = 1
    
    
    # get the info about the blocked IPs
    blocked_ips = subprocess.check_output(['iptables','-nvL','fw-open']).decode("utf-8")
    
    for ll in blocked_ips.splitlines():
        if 'DROP' in ll:
            cur_ip = ll.split()[7]
            cur_num = int(ll.split()[0])
            if cur_ip not in bl_ips:
                bl_ips[cur_ip]=[cur_num]
            else:
                bl_ips[cur_ip].append(cur_num)
     
    unbl_ips=[]
    
    for bl_ip in bl_ips:
        nn = bl_ips[bl_ip]
        if len(nn)>9:
            if nn[-1] == nn[-10]:
                subprocess.Popen(['iptables','-D','fw-open','-s',bl_ip,'-j','DROP'])
                new_log = new_log+'<b>Unbanning</b> {} <br>\n'.format(bl_ip)
                unbl_ips.append(bl_ip)
                iptables_changed = 1
    
    for mm in unbl_ips:
        bl_ips.pop(mm,None)
    
    #save iptables.rules if any change 
    if iptables_changed == 1:
        with open('/etc/iptables/iptables.rules', 'w') as output:
            iptables_save = subprocess.Popen('iptables-save', stdout=output)
            iptables_save.communicate()
        
        new_log = new_log+'Saving iptables.rules<br>'
    
    new_log = new_log+'Waiting....<br><br>\n\n'
    
    with open(log_file,"w") as ll:
        ll.write(new_log+old_log)
        
    with open(html_file,"w") as hh:
        hh.write(html_top)
        hh.write('<table>\n<tbody>\n')
        hh.write('<tr><td align="center">IP</td><td align="center" colspan="3">T[min]</td><td align="center">con</td></tr>')
        for ii in bl_ips:
            try:
                tm = (max(5*(len(bl_ips[ii])-1),1))/(bl_ips[ii][-1]-bl_ips[ii][0])
            except:
                tm = 0
            hh.write('<tr><td>{}</td><td>&nbsp;-&nbsp;</td><td>{:.2g}</td><td>&nbsp;&nbsp;-&nbsp;</td><td>{}</td><tr>\n'.format(ii,tm,", ".join(str(x) for x in bl_ips[ii])))
        hh.write('</tbody>\n</table>\n')
        hh.write("<br>----<br><br>\n\n"+new_log+old_log+html_end)
        
    time.sleep(300)
    
            