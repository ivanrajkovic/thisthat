#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import cv2
import time
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

GoogleAuth.DEFAULT_SETTINGS['client_config_file'] = os.path.join(os.path.expanduser('~'),'.credentials','client_secrets.json')

cred_file = os.path.join(os.path.expanduser('~'),'.credentials','cred.json')


while True:
    g_login = GoogleAuth()
    g_login.LoadCredentialsFile(cred_file)
    if g_login.credentials is None:
        # Authenticate if they're not there
        g_login.LocalWebserverAuth()
    elif g_login.access_token_expired:
    # Refresh them if expired
        print('refreshing gauth...')
        g_login.Refresh()
    else:
        # Initialize the saved creds
        g_login.Authorize()
    # Save the current credentials to a file
    g_login.SaveCredentialsFile(cred_file)


    drive = GoogleDrive(g_login)

    cc = cv2.VideoCapture(0)
    time.sleep(1)
    return_value, image = cc.read()
    del(cc)


    im_file = os.path.join(os.path.expanduser('~'),'im_temp','image-'+time.strftime("%Y%m%d_%H%M%S")+'.jpg')
    if not os.path.exists(os.path.dirname(im_file)):
        os.makedirs(os.path.dirname(im_file), exist_ok=True)

    cv2.imwrite(im_file,image)

    ff = drive.CreateFile(({'title':os.path.basename(im_file), 'mimeType':'image/jpg',"parents": [{"kind": "drive#fileLink","id": '1agChiRIxxO2BHZmnAyUxnZGULt1l-VOE'}]}))
    ff.SetContentFile(im_file)
    ff.Upload()

    os.remove(im_file)

    if len(sys.argv) > 1:
        print('Waiting till {}'.format(time.ctime(time.time()+float(sys.argv[1])*60)))
        time.sleep(float(sys.argv[1])*60)
    else:
        print('Waiting till {}'.format(time.ctime(time.time()+1800)))
        time.pause(1800)




## get folder and file ids
#file_list = drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
#for file1 in file_list:
#    print ('title: %s, id: %s' % (file1['title'], file1['id']))
