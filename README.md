# README #

Few projects for home use.

Newest fun project - kmines solver - is a python program to solve kmines (minesweeper clone). It does this in following steps:

1. get the kmines window geometry

2. open a random field around center

3. take a screenshot, read information about the mines

4. put all the possible mines and open all the no-mines fields

5. if not over, go back to step 3

see it in action:

https://www.youtube.com/watch?v=UnF9Mij4VpM


