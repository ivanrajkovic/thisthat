#!/usr/bin/env python
import os
import sys
from psutil import process_iter
import time
import numpy
import subprocess
import random
import select
import cv2
from PIL import ImageGrab
#import pyscreenshot as ImageGrab
#from pymouse import PyMouse
from pynput.mouse import Button, Controller
import matplotlib.pyplot as plt


class ms_field:
    def __init__(self,width,height):
        '''
        initialize empty kmines field
        bomb: -1 = unknown, 0 = no bomb, 1 = bomb
        neigh: number of neighbours around the field
        neigh_b: number of bombs in all neighbours (-1 = unknown)

        f_left - list of fields not open
        n_left - list of numbered fields not solved
        '''
        self.fields=numpy.zeros([width,height],dtype=numpy.dtype([('bomb', numpy.int8), ('neigh', numpy.int8), ('neigh_b', numpy.int8)]))
        self.f_left=[]
        self.n_left=[]
        self.x = width
        self.y = height

        self.fields['bomb']=-1
        self.fields['neigh_b']=-1
        self.fields['neigh']=8
        for edge in [0,-1]:
            self.fields[edge,:]['neigh']=5
            self.fields[:,edge]['neigh']=5

        for c1 in [0,-1]:
            for c2 in [0,-1]:
                self.fields[c1,c2]['neigh']=3

        for xx in range(width):
            for yy in range(height):
                self.f_left.append([xx,yy])


def km_winfo():
    '''
    get kmines process info if running
    start kmines if not running
    get window geometry
    '''
    for proc in process_iter():
        if proc.name() == 'kmines':
            kpid = proc.pid
            #print('already running')
            break
    else:
        #print('starting')
        kmprog = subprocess.Popen('kmines')
        kpid = kmprog.pid
        time.sleep(.7)

    try:
        kmw=subprocess.Popen(['xdotool','search','--pid',str(kpid)],stdout=subprocess.PIPE)
        a=int(kmw.communicate()[0])
    except:
        sys.exit('no kmines window')

    subprocess.Popen(['xdotool', 'windowactivate', str(a)],stdout=subprocess.PIPE)

    gg = subprocess.Popen(['xdotool', 'getwindowgeometry', str(a)],stdout=subprocess.PIPE)
    g_out  = gg.communicate()

    g_out = g_out[0].decode().split('\n')

    xx,yy = [int(i) for i in g_out[1].split()[1].split(',')]
    xw,yw = [int(i) for i in g_out[2].split()[1].split('x')]

    return([xx,yy,xw,yw])


def get_mesh(kgeo,msw,msh):
    '''
    get fileds position from kmines window geometry
    '''

    offset_top = 70
    offset_bottom = 50

    screenshot = numpy.array(ImageGrab.grab([kgeo[0],kgeo[1],kgeo[0]+kgeo[2],kgeo[1]+kgeo[3]]).convert('L'))

    sc = screenshot[offset_top:-offset_bottom,:]
    rr,sc_t= cv2.threshold(sc,150,255,cv2.THRESH_BINARY)

    ff,hh = cv2.findContours(sc_t,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    oo= cv2.HoughLinesP(sc_t,1,1*numpy.pi/180,150)


    ymin, xmin = sc.shape
    xmax=0
    ymax=0

    for gg in ff:
        xx,yy,ww,hh = cv2.boundingRect(gg)
        if ww < (sc.shape[1]-10) and hh < (sc.shape[0]-10):
            xmin=numpy.min([xmin,xx])
            ymin=numpy.min([ymin,yy])
            xmax=numpy.max([xmax,xx+ww])
            ymax=numpy.max([ymax,yy+hh])

    fhb = kgeo[0]+xmin+(xmax-xmin)/(2*msw)
    fvb = kgeo[1]+offset_top+ymin+(ymax-ymin)/(2*msh)
    db = ((xmax-xmin)/msw + (ymax-ymin)/msh)/2.01

    return(fhb,fvb,db)


def neig(xx,yy,ww,hh):
    '''
    get neighbour fields
    '''
    xmin = max(0,xx-1)
    ymin = max(0,yy-1)
    xmax = min(ww-1,xx+1)
    ymax = min(hh-1,yy+1)

    list=[]

    for oo in numpy.arange(xmin,xmax+1):
        for pp in numpy.arange(ymin,ymax+1):
            list.append([oo,pp])

    list.remove([xx,yy])
    return(list)


def neig_ns(xx,yy,kmf):
    '''
    get neighbour fields that are not solved
    '''
    xmin = max(0,xx-1)
    ymin = max(0,yy-1)
    xmax = min(kmf.x-1,xx+1)
    ymax = min(kmf.y-1,yy+1)

    list=[]

    for oo in numpy.arange(xmin,xmax+1):
        for pp in numpy.arange(ymin,ymax+1):
            if kmf.fields[oo,pp]['bomb']==-1:
                list.append([oo,pp])
    if [xx,yy] in list:
        list.remove([xx,yy])

    return(list)


def neig_b(xx,yy,kmf):
    '''
    get neighbour fields with bomb
    '''
    xmin = max(0,xx-1)
    ymin = max(0,yy-1)
    xmax = min(kmf.x-1,xx+1)
    ymax = min(kmf.y-1,yy+1)

    list=[]

    for oo in numpy.arange(xmin,xmax+1):
        for pp in numpy.arange(ymin,ymax+1):
            if kmf.fields[oo,pp]['bomb']==1:
                list.append([oo,pp])
    if [xx,yy] in list:
        list.remove([xx,yy])

    return(list)


def check_end(mm,kmf,screenshot,is_waiting):
    '''
    check if there is an end message
    '''
    eee = False
    crop_top = round(mm[1]+mm[2]*7)
    crop_left = round(mm[0]+mm[2]*13.5)
    crop_bottom = round(mm[1]+mm[2]*8)
    crop_right = round(mm[0]+mm[2]*16.5)


    end_check = numpy.array(screenshot.crop((crop_left,crop_top,crop_right,crop_bottom)).convert('L'))


    if numpy.median(end_check) < 80:
        eee =True
        if not is_waiting:
            at_end(mm,kmf)

    return(eee)


def at_end(mm,kmf):
    '''
    exit/show info/restart at end
    '''
    print('end\n')
    (print("'n' - stop playing; 'a' - show board info, 'w' - wait for input"))
    q,qq,qqq = select.select([sys.stdin], [], [], 5)
    if q:
        a = sys.stdin.readline()
        if a=='n\n':
            sys.exit('done')
        elif a=='a\n':
            show_map(mm,kmf)
            iii=input('waiting....')
            if not iii =='':
                sys.exit('done')
        elif a=='w\n':
            input('waiting.....')

    wait = check_end(mm,kmf,ImageGrab.grab(),True)
    while wait:
        print('waiting....')
        time.sleep(1)
        wait = check_end(mm,kmf,ImageGrab.grab(),True)

    main()


def random_click(mouse,mm,kmf):
    '''
    random click
    '''
    if random.random()>.5:
        random_click2(mouse,mm,kmf)
    else:
        random_click3(mouse,mm,kmf)


def random_click2(mouse,mm,kmf):
    '''
    random click anywhere on board's unsolved fields
    '''
    if len(kmf.f_left) > 0:
        rxx,ryy=random.choice(kmf.f_left)

        print('random')
        mouse_click(mouse,rxx,ryy,mm,Button.left)


def random_click3(mouse,mm,kmf):
    '''
    click next to a random board's unsolved field
    '''
    if len(kmf.n_left) == 0:
        random_click2(mouse,mm,kmf)
    else:

        xx,yy=random.choice(kmf.n_left)

        nsl = neig_ns(xx,yy,kmf)

        if nsl:
            print('random')
            rxx,ryy=random.choice(nsl)
            mouse_click(mouse,rxx,ryy,mm,Button.left)

        else:
            random_click(mouse,mm,kmf)


def first_click(mouse,mm,kmf):
    '''
    random click close to the board center
    '''
    print('\nstart')
    rxx = random.randrange(int(2*kmf.x/5),int(3*kmf.x/5))
    ryy = random.randrange(int(2*kmf.y/5),int(3*kmf.y/5))

    mouse_click(mouse,rxx,ryy,mm,Button.left)


def mouse_click(mouse,xx,yy,mm,but):
    '''
    mouse click, field number to pixels
    '''
    #mouse.click(round(mm[0])+round(mm[2]*xx),round(mm[1])+round(mm[2]*yy),but)
    mouse.position = (round(mm[0])+round(mm[2]*xx),round(mm[1])+round(mm[2]*yy))
    mouse.click(but)

def update_board(mm,kmf):
    '''
    update board from a screenshot
    '''
    time.sleep(.01)
    sshot=ImageGrab.grab()
    check_end(mm,kmf,sshot,False)

    screenshot = numpy.array(sshot)
    qqq = [a for a in kmf.f_left]

    for xx,yy in qqq:
            crop_top = round(mm[1]+mm[2]*(yy-0.3))
            crop_left = round(mm[0]+mm[2]*(xx-0.3))
            crop_bottom = round(mm[1]+mm[2]*(yy+0.4))
            crop_right = round(mm[0]+mm[2]*(xx+0.4))

            num_field = screenshot[crop_top:crop_bottom,crop_left:crop_right]

            if numpy.all(num_field[5:-5,5:-5] == num_field[6,6]):
                if (num_field[6,6] == [211, 215, 207]).all():
                    kmf.fields[xx,yy]['bomb']=0
                    kmf.fields[xx,yy]['neigh_b']=0
                    if [xx,yy] in kmf.f_left:
                        kmf.f_left.remove([xx,yy])

            else:
                kmf.fields[xx,yy]['bomb']=0
                kmf.n_left.append([xx,yy])
                if [xx,yy] in kmf.f_left:
                    kmf.f_left.remove([xx,yy])
                for qq in num_field.reshape(num_field.shape[0]*num_field.shape[1],3):
                    if (qq == [0, 0, 255]).all():
                        kmf.fields[xx,yy]['neigh_b'] = 1
                        break
                    elif (qq == [0, 128, 0]).all():
                        kmf.fields[xx,yy]['neigh_b'] = 2
                        break
                    elif (qq == [128, 128, 0]).all():
                        kmf.fields[xx,yy]['neigh_b'] = 3
                        break
                    elif (qq == [128, 0, 128]).all():
                        kmf.fields[xx,yy]['neigh_b'] = 4
                        break
                    elif (qq == [255, 0, 0]).all():
                        kmf.fields[xx,yy]['neigh_b'] = 5
                        break
                    elif (qq == [128, 0, 0]).all():
                        kmf.fields[xx,yy]['neigh_b'] = 6
                        break
                    elif (qq == [0,128, 128]).all():
                        kmf.fields[xx,yy]['neigh_b'] = 7
                        break
                    elif (qq == [43, 0, 0]).all():
                        kmf.fields[xx,yy]['neigh_b'] = 8
                        break
                else:
                    at_end(mm,kmf)

    return(kmf)


def next_move(mouse,kmf,mm):
    '''
    find all next moves
    '''
    c=True
    moves = False
    while c:
        qqq = kmf.n_left.copy()
        if len(qqq) > 0:

            for pp in qqq:
                kmf, c = check_field(mouse,pp[0],pp[1],kmf,mm)
                if c:
                    moves = True
        else:
            c=False

    return(moves,kmf)


def check_field(mouse,xx,yy,kmf,mm):
    '''
    find a move next to a given field
    '''
    c=False
    nn = neig(xx,yy,kmf.x,kmf.y)
    # not solved fields
    ns = neig_ns(xx,yy,kmf)
    ns_count = len(ns)
    # solved fields around:
    sf_count = len(nn) - ns_count
    # fields with bombs
    bf_count = len(neig_b(xx,yy,kmf))


    if bf_count == kmf.fields[xx,yy]['neigh_b']:
        for xb, yb in ns:
            kmf.fields[xb,yb]['bomb']=0
        if ns_count>0:
            mouse_click(mouse,xx,yy,mm,Button.middle)
        c=True
        if [xx,yy] in kmf.f_left:
            kmf.f_left.remove([xx,yy])
        kmf.n_left.remove([xx,yy])

    elif ns_count == (kmf.fields[xx,yy]['neigh_b']-bf_count):
        for xb, yb in ns:
            mouse_click(mouse,xb,yb,mm,Button.right)
            kmf.fields[xb,yb]['bomb']=1
            kmf.f_left.remove([xb,yb])
        c=True
        if [xx,yy] in kmf.f_left:
            kmf.f_left.remove([xx,yy])
        kmf.n_left.remove([xx,yy])

    else:
        ns_neig=[]
        for xb,yb in ns:
            for aa in neig(xb,yb,kmf.x,kmf.y):
                if (not aa in ns_neig) and (kmf.fields[aa[0],aa[1]]['neigh_b']>0) and (not (aa==[xx,yy])):
                    ns_neig.append(aa)

        # add bombs
        for xb,yb in ns_neig:
            nnn = neig_ns(xb,yb,kmf)
            nbf = len(nnn)

            count = 0
            for nx, ny in ns:
                if [nx,ny] in nnn:
                    count+=1
                    nnn.remove([nx,ny])

            if kmf.fields[xb,yb]['neigh_b'] - len(neig_b(xb,yb,kmf)) - (kmf.fields[xx,yy]['neigh_b'] - bf_count)  == len(nnn):
                for nx,ny in nnn:
                    c=True
                    mouse_click(mouse,nx,ny,mm,Button.right)
                    kmf.fields[nx,ny]['bomb']=1
                    kmf.f_left.remove([nx,ny])

        # click on no-bombs
        for xb,yb in ns_neig:
            nns = ns.copy()
            # nnm is the same as nnn
            nnm = neig_ns(xb,yb,kmf)

            for nx, ny in ns:
                if [nx,ny] in nnm:
                    nnm.remove([nx,ny])
                    nns.remove([nx,ny])

            if (len(nns)==0) and (0 == (kmf.fields[xb,yb]['neigh_b'] - len(neig_b(xb,yb,kmf))  -  (kmf.fields[xx,yy]['neigh_b'] - bf_count) )):
                for nx,ny in nnm:
                    c=True
                    mouse_click(mouse,nx,ny,mm,Button.left)
                    kmf.fields[nx,ny]['bomb']=0

    return(kmf,c)


def start_new(mouse,gg):
    '''
    start a new game
    '''
    #mouse.click(gg[0]+45,gg[1]+18)
    mouse.position = (gg[0]+45,gg[1]+18)
    mouse.click(Button.left)

def show_map(mm,kfm):
    '''
    show kmines board with fields info
    '''
    screenshot = numpy.array(ImageGrab.grab([int(mm[0]-mm[2]/2),int(mm[1]-mm[2]/2),int(mm[0]+mm[2]*kfm.x-mm[2]/2),int(mm[1]+mm[2]*kfm.y-mm[2]/2)]))

    plt.rcParams["figure.figsize"] = (12,12)
    plt.imshow(screenshot)
    for aa in kfm.n_left:
        plt.text(int((aa[0]+.5)*mm[2]-15),int((aa[1]+.5)*mm[2]),'X',color='red')
    for aa in kfm.f_left:
        plt.text(int((aa[0]+.5)*mm[2]-15),int((aa[1]+.5)*mm[2]+15),'O',color='blue')
    for ii in numpy.arange(kfm.x):
        for jj in numpy.arange(kfm.y):
            plt.text(int((ii+.5)*mm[2]),int((jj+.5)*mm[2]),kfm.fields[ii,jj]['neigh_b'],color='magenta')
            plt.text(int((ii+.5)*mm[2]),int((jj+.5)*mm[2]+15),kfm.fields[ii,jj]['bomb'],color='green',fontweight='bold')

    plt.show()


def main():
    '''
    play game
    '''
    kmf_w=30
    kmf_h=16
    #mouse=PyMouse()
    mouse=Controller()
    km_field = ms_field(kmf_w,kmf_h)
    kmw_geom = km_winfo()
    time.sleep(.1)
    km_mesh = get_mesh(kmw_geom,km_field.x,km_field.y)
    start_new(mouse,kmw_geom)
    time.sleep(.6)
    first_click(mouse,km_mesh,km_field)
    time.sleep(.02)
    while 1:
        km_field = update_board(km_mesh,km_field)
        moves,km_field = next_move(mouse,km_field,km_mesh)

        if not moves:
            random_click(mouse,km_mesh,km_field)


if __name__=="__main__":
    main()
