#!/usr/bin/env python
import urllib.request
import fileinput
import sys
import subprocess

hfile='https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt'
dfile='https://raw.githubusercontent.com/notracking/hosts-blocklists/master/domains.txt'

dloc = '/etc/dnsmasq_block/domains.txt'
hloc = '/etc/dnsmasq_block/hostnames.txt'

urllib.request.urlretrieve(hfile,hloc)
urllib.request.urlretrieve(dfile,dloc)

for line in fileinput.input(dloc,inplace=1):
    if 'a2z.com' not in line:
        sys.stdout.write(line)

subprocess.call(['systemctl','restart','dnsmasq_block.service'])



