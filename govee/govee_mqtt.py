#!/usr/bin/env python

import subprocess
import os
#from inotify_simple import INotify, flags
import numpy
import paho.mqtt.client as paho
import time
import glob

if __name__ == "__main__":

    gd = '/tmp/govee/'
    mqtt_server = '192.168.12.32'
    mqtt_port = 1883

    os.makedirs(gd, exist_ok=True)


    while 1:
        gg = subprocess.Popen(['goveebttemplogger','-l',gd,'-t','55'],stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        time.sleep(120)
        gg.terminate()


        #inotify = INotify()
        #watch_flags = flags.MODIFY
        #wd = inotify.add_watch(gd, watch_flags)

        #while 1:
        #    for event in inotify.read():
        #        ff = os.path.join(gd,event.name)
        ffs = glob.glob(gd+'*txt')
        print(ffs)
        for ff in ffs:
            if 'gvh' in ff:
                try:
                    dd=numpy.genfromtxt(ff,usecols=(2,3,4))
                    os.rename(ff,'/tmp/govee.txt')
                    if dd.ndim==1:
                        temp, humi, batt = dd[:]
                    else:
                        temp, humi, batt = dd.mean(axis=0)
                    mqtt_cl= paho.Client("govee")
                    mqtt_cl.connect(mqtt_server,mqtt_port)
                    mqtt_cl.publish("govee/temp",round(90*temp/5)/10+32)
                    mqtt_cl.publish("govee/humi",round(humi))
                    mqtt_cl.publish("govee/batt",batt)
                    mqtt_cl.disconnect()
                except:
                    pass
